import { shallow } from "enzyme";
import React from "react";

import { Loader } from "../../../src/components/Loader";
import { Spinner } from "../../../src/components/Loader/components/Spinner";

it("shows a Spinner component inside of it", () => {
  const subject = shallow(<Loader phrase="foo" />);

  expect(subject.find(Spinner)).toHaveLength(1);
});

it("shows the injected phrase on the expected element", () => {
  const subject = shallow(<Loader phrase="Phrase to display while loading" />);

  expect(subject.find('[data-test-hook="phrase"]').text()).toEqual(
    "Phrase to display while loading"
  );
});
