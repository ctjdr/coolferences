import { DateJson } from "../entities/api/DateJson";
import { LocationJson } from "../entities/api/LocationJson";

export interface Conference {
  cfpUrl: string;
  cocUrl: string;
  date: DateJson;
  hasCfp: boolean;
  hasCoc: boolean;
  isConcluded: boolean;
  location: LocationJson;
  name: string;
  url: string;
}
